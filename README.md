## Chat App using SocketIO

This is a simple but cool app designed to demonstrate the use of SocketIO to make realtime requests and updates.

### Usage

```bash
npm i
npm run start
```
Navigate to localhost:3000

